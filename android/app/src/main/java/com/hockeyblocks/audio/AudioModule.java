package com.hockeyblocks.audio;

import android.content.Context;
import android.media.MediaPlayer;
import com.hockeyblocks.R;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;


public class AudioModule extends ReactContextBaseJavaModule {
    private MediaPlayer mediaPlayer;
    private final Context context;

    public AudioModule(ReactApplicationContext reactApplicationContext)
    {
        super(reactApplicationContext);
        context = getReactApplicationContext();
        mediaPlayer = MediaPlayer.create(context, R.raw.music);
    }

    @ReactMethod
    public void play()
    {
        mediaPlayer.start();
        mediaPlayer.setLooping(true);
    }

    @ReactMethod
    public void setVolume(float volume)
    {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.setVolume(volume, volume);
        }
    }

    @Override
    public String getName()
    {
        return "audio";
    }
}
