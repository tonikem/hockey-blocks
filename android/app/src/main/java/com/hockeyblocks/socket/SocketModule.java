package com.hockeyblocks.socket;

import android.content.Context;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.io.*;
import java.security.*;
import javax.net.ssl.*;
import java.security.cert.*;


public class SocketModule extends ReactContextBaseJavaModule {
    private KeystoreSetup keystoreSetup;
    private static int unique_number;
    public SocketClient socketClient;
    private final Context context;

    SocketModule(ReactApplicationContext reactApplicationContext, int unique_numer)
            throws IOException, NoSuchAlgorithmException, KeyManagementException,
            KeyStoreException, UnrecoverableKeyException, CertificateException
    {
        super(reactApplicationContext);

        this.unique_number = unique_numer;

    // Basic React Native app context.
        this.context = reactApplicationContext;

    // Initializing the `KeystoreSetup` without blocking.
        new Thread(new Runnable() {
            public void run() {
                try {
                    keystoreSetup = new KeystoreSetup(context);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public String getName()
    {
        return "socket";
    }

    @ReactMethod
    public void start(final Callback callback) // Lets the RN thread continue.
    {
        new Thread(
            new Runnable() {
                public void run() {
                    try {
                        socketClient = keystoreSetup.initialize();
                        socketClient.run(unique_number, callback);
                    }
                    catch(Exception e) {
                        callback.invoke("err");
                        socketClient.close();
                        e.printStackTrace();
                    }
                }
            }
        ).start();
    }

    @ReactMethod
    public void send(double x, double y, double px, double py, Callback callback)
    {
        try {
            socketClient.send(x, y, px, py);
            callback.invoke("ok");
        }
        catch (IOException ioe) {
            callback.invoke("err");
        }
        catch (Exception e) {
        // Close the connection.
            this.stop();
        }
    }

    @ReactMethod
    public void isConnected(Callback callback)
    {
        try {
            socketClient.check(callback);
        }
        catch(NullPointerException npe) {
        // if socket has not been created.
            callback.invoke(false);
        }
        catch (Exception e) {
        // Close the connection.
            this.stop();
        }
    }

    @ReactMethod
    public void receiveOpponentX(Callback callback)
    {
        try {
            socketClient.receiveOpponentX(callback);
        }
        catch(NullPointerException npe) {
        // if socket is closed and null.
            callback.invoke("err");
        }
        catch (Exception e) {
        // Close the connection.
            this.stop();
        }
    }

    @ReactMethod
    public void receiveOpponentY(Callback callback)
    {
        try {
            socketClient.receiveOpponentY(callback);
        }
        catch (Exception e) {
            // Close the connection.
            this.stop();
        }

    }

    @ReactMethod
    public void receivePuckX(Callback callback)
    {
        try {
            socketClient.receivePuckX(callback);
        }
        catch (Exception e) {
        // Close the connection.
            this.stop();
        }
    }

    @ReactMethod
    public void receivePuckY(Callback callback)
    {
        try {
            socketClient.receivePuckY(callback);
        }
        catch (Exception e) {
        // Close the connection.
            this.stop();
        }
    }

    @ReactMethod
    public void setScreenSize(int x, int y)
    {
        try {
            socketClient.setScreen(x, y);
        }
        catch (Exception e) {
        // Close the connection.
            this.stop();
        }
    }

    @ReactMethod
    public void getThreadID(Callback callback)
    {
        try {
            socketClient.getThreadID(callback);
        }
        catch (Exception e) {
        // Close the connection.
            this.stop();
        }
    }

    @ReactMethod
    public void changeEngineHost()
    {
        try {
            socketClient.changeEngineHost(true);
        }
        catch (Exception e) {
        // Close the connection.
            this.stop();
        }
    }

    @ReactMethod
    public void stop()
    {
        try {
            socketClient.close();
            socketClient = null;
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    @ReactMethod
    public void reload() {
        keystoreSetup = new KeystoreSetup(context);
    }

    @ReactMethod
    public void exit() {
        System.exit(0);
    }
}
