package com.hockeyblocks.socket;

import android.content.Context;

import java.io.*;
import java.security.*;
import java.security.cert.*;
import javax.net.ssl.*;

import com.hockeyblocks.socket.SocketClient;
import com.hockeyblocks.R;


public class KeystoreSetup {
    private final Context context;
    private char[] password;

    public KeystoreSetup(Context context)
    {
        this.context = context;
    }

    public SocketClient initialize()
        throws IOException, NoSuchAlgorithmException, KeyManagementException,
        KeyStoreException, UnrecoverableKeyException, CertificateException
    {
    // KeyStore password to char array.
        InputStream inputStream = context.getResources().openRawResource(R.raw.password);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        password = reader.readLine().toCharArray();

    // KeyStore and TrustStore files from assets.
        InputStream trustStoreStream = context.getResources().openRawResource(R.raw.truststore);
        InputStream keyStoreStream = context.getResources().openRawResource(R.raw.keystore);

    // Setting up TrustStore.
        KeyStore trustStore = KeyStore.getInstance("PKCS12");
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                KeyManagerFactory.getDefaultAlgorithm()
        );
        trustStore.load(trustStoreStream, password);
        trustManagerFactory.init(trustStore);

    // Setting up KeyStore.
        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(
                KeyManagerFactory.getDefaultAlgorithm()
        );
        keyStore.load(keyStoreStream, password);
        keyManagerFactory.init(keyStore, password);

    // Initializing ssl context.
        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(
                keyManagerFactory.getKeyManagers(),
                trustManagerFactory.getTrustManagers(),
                new SecureRandom() // <- Replaced null.
        );

    // Finally declaring SSLSocket and initializing `socketClient`.
        SSLSocketFactory factory = sslContext.getSocketFactory();

        return new SocketClient(factory);
    }
}
