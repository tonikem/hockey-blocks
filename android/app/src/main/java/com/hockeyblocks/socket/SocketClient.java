package com.hockeyblocks.socket;

import com.facebook.react.bridge.Callback;

import java.io.*;
import java.net.*;
import javax.net.ssl.*;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


public class SocketClient {
    private final SSLSocketFactory factory;
    private DataOutputStream output = null;
    private DataInputStream input = null;
    public SSLSocket connection = null;

// Used to check if connection is ready.
    private boolean socketIsConnected = false;

// Screen width and height measured in RN.
    public short screen_width, screen_height;

// Opponent's coordinates. Both Must be puplic,
// because Java doesn't allow pass-by-reference.
    public int OpponentX, OpponentY;

// `thread_id` indicates if client hosts the physics engine.
    public boolean thread_id, change_host;

// Puck's received coordinates.
    public int PuckX, PuckY;

    public SocketClient(SSLSocketFactory factory)
    {
        this.factory = factory;
    }

    public void run(int unique, Callback callback) throws IOException, Exception // <- first method.
    {
        this.connection = (SSLSocket) factory.createSocket("85.23.247.189", 3010);
        this.connection.setUseClientMode(true);

    // Enabling ciphers. The weak ones are commented out.
        this.connection.setEnabledCipherSuites( new String[] {
            // ChaCha based ciphers are not supported.
                "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256",
                "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384",
                "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
                "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA",
                "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA",
                "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA",
              //"TLS_RSA_WITH_AES_128_GCM_SHA256",
              //"TLS_RSA_WITH_AES_256_GCM_SHA384",
              //"TLS_RSA_WITH_AES_128_CBC_SHA",
              //"TLS_RSA_WITH_AES_256_CBC_SHA"
        });

        this.output = new DataOutputStream(connection.getOutputStream());
        this.input = new DataInputStream(connection.getInputStream());

        this.connection.startHandshake(); // Waiting for handshake to be carried successfully.

        Random random = new Random();
        random.setSeed(System.currentTimeMillis());

        long seeded_random = ThreadLocalRandom.current().nextLong(542551296285575047L);
        boolean bool_random = ThreadLocalRandom.current().nextBoolean();

        seeded_random *= bool_random ? 17 : -17;
        this.output.writeLong(seeded_random);
        this.output.flush();

    // The `thread_id` given by the server.
        thread_id = input.readBoolean(); // <- DON'T MOVE THIS!!

    // Tests if the client has connected with itself.
        this.output.writeInt(unique);
        if (unique == this.input.readInt()) {
            this.close(); throw new Exception("Double connection!");
        }

    // RN will now see that the connection was successful.
        this.socketIsConnected = true; // <- Used in callback.

        callback.invoke("ok");

    // Thread will now read the coordinates sent by other client.
        new Thread( new Runnable() {
            public void run() {
                try {
                    while(true)
                    {// Subtracting the received value from the screen dimensions gives
                     // us the mirrored coordinates, as they should appear to the player.
                        OpponentX = screen_width - screen_width / 6
                            - Math.round( (input.readShort() * screen_width) / 10000 );

                        OpponentY = screen_height - screen_width / 6
                            - Math.round( (input.readShort() * screen_height) / 10000 );

                        PuckX = screen_width - screen_width / 6
                            - Math.round( (input.readShort() * screen_width) / 10000 );

                        PuckY = screen_height - screen_width / 6
                            - Math.round( (input.readShort() * screen_height) / 10000 );
                    }
                }
                catch (NullPointerException npe) {
                    // Let it fail.
                }
                catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();  // Runs forever..
    }

// Next we have functions for checking connection.
    public void check(Callback callback)
    {
        callback.invoke(this.socketIsConnected);
    }

    public void setScreen(int width, int height)
    {
        screen_width = (short) width;
        screen_height = (short) height;

    // We also set initial coordinates for opponent.
    //(player's and opponent's size is 1/6 of screen width)
        OpponentX = (screen_width / 2) - (screen_width / 12);
        OpponentY = (screen_height / 3) - (screen_width / 6);

    // Also sets the pucks initial coordinates.
        PuckX = (screen_width / 2 - screen_width / 12);
        PuckY = (screen_height / 2 - screen_width / 12);
    }

// Values (x, y, r) are now converted relative
// to the screen size and casted to type short.
    public void send(double x, double y, double px, double py) throws IOException
    {
        this.output.writeShort((short) Math.round((x / screen_width) * 10000));
        this.output.writeShort((short) Math.round((y / screen_height) * 10000));
        this.output.writeShort((short) Math.round((px / screen_width) * 10000));
        this.output.writeShort((short) Math.round((py / screen_height) * 10000));
    }

    public void receiveOpponentX(Callback callback)
    {
        callback.invoke(Integer.toString(this.OpponentX));
    }

    public void receiveOpponentY(Callback callback)
    {
        callback.invoke(Integer.toString(this.OpponentY));
    }

    public void receivePuckX(Callback callback)
    {
        callback.invoke(Integer.toString(this.PuckX));
    }

    public void receivePuckY(Callback callback)
    {
        callback.invoke(Integer.toString(this.PuckY));
    }

    public void getThreadID(Callback callback)
    {
        callback.invoke(thread_id);
    }

    public synchronized void changeEngineHost(boolean bool)
    {
        change_host = bool;
    }

    public void close()
    {
    // Setting these to their initial values.
        OpponentX = (screen_width / 2) - (screen_width / 12);
        OpponentY = (screen_height / 3) - (screen_width / 6);
        PuckX = (screen_width / 2 - screen_width / 12);
        PuckY = (screen_height / 2 - screen_width / 12);

        try {
        // Closing connection to the server.
            if (this.input != null) this.input.close();
            if (this.output != null) this.output.close();
            if (this.connection != null) this.connection.close();
            this.connection = null;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
