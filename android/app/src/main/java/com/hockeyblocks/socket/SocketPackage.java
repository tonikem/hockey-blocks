package com.hockeyblocks.socket;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.JavaScriptModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;


public class SocketPackage implements ReactPackage {
    protected static int unique;

// Method for reloading new SocketMocule.
    protected static void reloadSocketMocule() {

    }

    public SocketPackage(int unique_number) {
        this.unique = unique_number;
    }

    public List<Class<? extends JavaScriptModule>> createJSModules()
    {
        return Collections.emptyList();
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactApplicationContext)
    {
        return Collections.emptyList();
    }

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactApplicationContext)
    {
        List<NativeModule> modules = new ArrayList<>();

        try {
            SocketModule socketModule = new SocketModule(reactApplicationContext, unique);

            if (modules.contains(socketModule)) {
                socketModule.stop(); // Closing dublicate.
                modules.remove(socketModule);
            }
            modules.add(socketModule);

        }
        catch(IOException | NoSuchAlgorithmException | KeyManagementException |
            KeyStoreException | UnrecoverableKeyException | CertificateException e)
        {
            e.printStackTrace();
        }
        catch(Exception e) {
            System.out.println("Unexpected error occured.");
            e.printStackTrace();
        }

        return modules;
    }
}
