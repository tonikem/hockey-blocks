/** @format */

import React from 'react';
import { Provider } from 'react-redux';
import { AppRegistry } from 'react-native';
import App from './src/components/App';
import { name } from './src/components/app.json';
import SplashScreen from 'react-native-splash-screen'

import store from './src/redux_store/store';


class HockeyBlocks extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <App/>
            </Provider>
        )
    }
}

AppRegistry.registerComponent(
    name, () => HockeyBlocks
);
