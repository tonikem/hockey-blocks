<h1>Hockey Blocks</h1>

<img src="https://i.imgur.com/mH3GzIY.png">
<img src="https://i.imgur.com/5xNeMq1.png">

<h2>Android pohjainen moninpeli</h2>

Peli muistuttaa toiminnaltaan perinteistä ilmakiekkoa. Tarkoitus on lyödä kiekkoa kentällä ja saada vastapuolen maaliin.

HUOM! peli on vielä hyvin keskeneräisessä vaiheessa pelattavuuden kannalta, mutta palvelimen ja laitteiden välinen ytheys toimii.


<h3>Kuinka moninpeli toimii</h3>

Peli alkaa kun kaksi pelaajaa yrittää ja onnistuneesti yhdistää palvelimeen samaan aikaan. Palvelin on luotu Java:lla, sillä sen yhdistäminen Android:in kanssa on näin vaivattominta.

Palvelin avaa Socket-object:ja sitä mukaa, kun pelaajia liittyy. Itse palvelin toimii ainoastaan tiedonvälittäjänä kahden pelaajan välillä. Pelimoottoria ylläpitää jompi kumpi pelaajista, jolta vastapelaaja saa vastaanotettua koordinaatteja.

Vastapelaajan puolella luonnollisesti tapahtuu viivettä, johtuen mm. TCP-protokollan hitaudesta, pelimoottorin sijainnista sekä ReactNativen ja Javakoodin hitaasta kommunikoinnista keskenään (asynkroninen).
