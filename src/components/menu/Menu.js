/**
 * This is the component that renders first and is used
 * for every action made before the actual game starts.
 * */

import React from 'react';
import { connect } from "react-redux";
import LinearGradient from "react-native-linear-gradient";

import socket from "../connection/socket";

import Main from './views/MainView';
import { Start, Online, Single, Settings, About } from './views/Views';
import menu from '../../styles/menu/menu';


class Menu extends React.Component {
// This whole component is based on one
// function that serves different sub-menu
// components such as `Start` and `Settings`.

    render() {

        const menuSetter = this.props.menuSetter.bind(this);
        const update = this.forceUpdate.bind(this);

        const MenuSelector = ({menu_state}) => {

            switch(menu_state) {

            case 'Main':
                return <Main menuSetter={menuSetter} update={update}/>;

            case 'Start':
                return <Start menuSetter={menuSetter} update={update}/>;

            case 'Online':
                return <Online menuSetter={menuSetter} update={update}/>;

            case 'Single':
                return <Single menuSetter={menuSetter} update={update}/>;

            case 'Settings':
                return <Settings menuSetter={menuSetter} update={update}/>;

            case 'About':
                return <About menuSetter={menuSetter} update={update}/>;

            case 'Exit':
                try {
                    socket.stop(); // <- Close the connection.
                    socket.exit(); // native call `System.exit(0)`
                } catch (e) {
                    // Do nothing.
                } break;

            default:
                return <Main menuSetter={menuSetter} update={update}/>;
            }
        };

        return (
            <LinearGradient
                style={menu.mainpage}
                locations={[0, 0.3, 0.5, 0.7]}
                start={{x: 0.2, y: 0.2}} end={{x: 0.8, y: 0.8}}
                colors={['#27dcfa', '#32bbe9', '#3a97d7', '#2572cc']}>
                <MenuSelector menu_state={this.props.menu_state.menu}/>
            </LinearGradient>
        );

    }
}


/* Redux-store methods */

const mapStateToProps = state => {
    return {
        menu_state: state.menu_state
    }
};

const mapDispatchToProps = dispatch => {
    const menuSetter = menu_state => dispatch({
        type: 'MENU_STATE',
        menu: menu_state
    });
    return {
    // Imported to `Menu` via props.
        menuSetter: menuSetter
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Menu);
