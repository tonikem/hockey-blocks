/**
 * This module is the main view for the starting menu.
 * There are 4 buttons in order from top to bottom:
 *
 *     Start
 *     Tutorial
 *     Settings
 *     Exit
 *
 * Each button takes functions: `menuSetter`, `update`
 * and string values: `nextView`, `label` as props.
 * */

import React from "react";
import { View } from "react-native";

import menu from "../../../styles/menu/menu";
import MenuButton from "../../buttons/menu_basic/MenuButton";


class Main extends React.PureComponent {
    render() {
        return (
            <View style={menu.view}>

                <MenuButton
                    setter={this.props.menuSetter}
                    update={this.props.update}
                    nextView="Start" label="Start"
                />

                <MenuButton
                    setter={this.props.menuSetter}
                    update={this.props.update}
                    nextView="Tutorial" label="Tutorial"
                />

                <MenuButton
                    setter={this.props.menuSetter}
                    update={this.props.update}
                    nextView="Settings" label="Settings"
                />

                <MenuButton
                    setter={this.props.menuSetter}
                    update={this.props.update}
                    nextView="Exit" label="Exit"
                />

            </View>
        )
    }
}

export default Main;
