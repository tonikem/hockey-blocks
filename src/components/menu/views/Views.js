import Start from './StartView';
import Online from './OnlineView';
import Single from './SingleView';
import Settings from './SettingsView';
import About from './AboutView';


// Exporting all views from this folder in one set.

module.exports = {
    Start, Online, Single, Settings, About
};
