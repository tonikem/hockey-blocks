import React from "react";
import { View } from "react-native";

import menu from "../../../styles/menu/menu";
import MenuButton from "../../buttons/menu_basic/MenuButton";
import CancelButton from "../../buttons/menu_basic/CancelButton";


class Start extends React.PureComponent {
    render() {
        return (
            <View style={menu.view}>

                <MenuButton
                    setter={this.props.menuSetter}
                    update={this.props.update}
                    nextView="Online" label="Online"
                />

                <MenuButton
                    setter={this.props.menuSetter}
                    update={this.props.update}
                    nextView="Single" label="Single"
                />

                <CancelButton
                    setter={this.props.menuSetter}
                    update={this.props.update}
                    nextView="Main" label="↩ Back "
                />

            </View>
        );
    }
}

export default Start;
