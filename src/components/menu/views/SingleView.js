/**
 * Single player mode.
 * */

import React from 'react';
import { connect } from "react-redux";
import { View, Text } from 'react-native';
import CancelButton from "../../buttons/menu_basic/CancelButton";


class Single extends React.Component {
    render() {
        return (
            <View>
                <Text>Single view</Text>

                <CancelButton
                    setter={this.props.menuSetter}
                    update={this.props.update}
                    nextView="Start" label="Cancel"
                />
            </View>
        );
    }
}


/* Redux-store methods */

const mapDispatchToProps = dispatch => {
    return {
        // :.
    };
};

export default connect(
    null, mapDispatchToProps
)(Single);
