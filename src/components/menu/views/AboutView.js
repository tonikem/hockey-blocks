import React from "react";
import { View } from "react-native";

import menu from "../../../styles/menu/menu";
import AboutPage from "../../about/AboutPage";


class About extends React.PureComponent {
    render() {
        return (
            <View style={menu.view}>
                <AboutPage
                    menuSetter={this.props.menuSetter}
                    update={this.props.update}
                />
            </View>
        );
    }
}

export default About;
