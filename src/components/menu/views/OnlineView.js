/**
 * This view pops up after online game is selected.
 * On default there is a loading animation and a timer
 * set to 30s. After that the player is brought back to
 * `StartView` along with message about connection timeout.
 * */

import React from 'react';
import { connect } from "react-redux";
import { View, Text } from 'react-native';

import start from '../../../styles/menu/start';
import Loader from '../../objects/menu/Loader';
import CancelButton from "../../buttons/menu_basic/CancelButton";
import socket from "../../connection/socket";


class Online extends React.Component {
/* private */ loading;
/* private */ checker;
/* private */ timer;

    constructor() {
        super();
        this.state = {
            load: "",
            ready: false
        };
    }

    componentDidMount() {
        this.setState({ ready: true });

        this.loading = setInterval( () => {
        // Every 500 ms one dot is added to loading text. The loop
        // goes one like so: ".", "..", "..." and Resets after 3rd dot.
            this.setState({load: this.state.load.length < 3 ? this.state.load + "." : "" })
        }, 500);

    // Timout to set interval to check available games.
        setTimeout( () => {
            this.checker = setInterval( () => {
                socket.isConnected( bool => {
                    if (bool === true) {
                        this.props.startGame()
                    }
                })
            }, 200);
        }, 1100);

        const quit = () => {
            try {
                socket.stop(); // <- Close the connection.
            } catch (e) {
                // Do nothing.
            }
            this.props.menuSetter('Start');
            this.props.update()
        };

    // Timeout to exit back to menu after too many attempts.
        this.timer = setTimeout( () => quit(), 30000);

        socket.start( callback => {
            if (callback === "err") quit();
        });
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
        clearInterval(this.loading);
        clearInterval(this.checker);
    }

    render() {
        return (
            <View style={start.view}>

                <Text style={start.loading}>
                    Connecting{this.state.load}
                </Text>

                <Loader/>

                <CancelButton
                    setter={this.props.menuSetter}
                    update={this.props.update}
                    nextView="Start" label="Cancel"
                />

            </View>
        );
    }
}


/* Redux-store methods */

const mapDispatchToProps = dispatch => {
    return {
        startGame: () => {
        // Leaves menu. Starts game.
            dispatch({
                type: 'START_GAME'
            });

        // Also sets menu back to Main.
            dispatch({
                type: 'MENU_STATE',
                menu: 'Main'
            });
        }
    };
};

export default connect(
    null, mapDispatchToProps
)(Online);
