import React from 'react';
import { connect } from 'react-redux';
import { View, Text, Slider,
    TouchableOpacity, Image } from "react-native";

import menu from "../../../styles/menu/menu";
import settings from "../../../styles/menu/settings";
import CancelButton from "../../buttons/menu_basic/CancelButton";
import MenuButton from "../../buttons/menu_basic/MenuButton";


class Settings extends React.PureComponent {
    render() {
        return (
            <View style={menu.view}>

                <Text style={settings.text}>
                    Volume: {this.props.settings.volume}
                </Text>

                <Slider style={settings.slider}
                        value={ parseFloat(
                            this.props.settings.volume
                        )}
                        onSlidingComplete={ value => {
                            this.props.volumeSetter(value)
                        }}
                        minimumValue={0.00}
                        maximumValue={1.00}
                />

                <Text style={settings.text}>
                    Difficulty: {this.props.settings.difficulty}
                </Text>



                <View style={settings.container}>
                    <Text style={settings.text}>Player: </Text>

                    <TouchableOpacity onPress={() => { }}>
                        <View elevation={1} style={settings.small_button}>
                            <Image style={settings.player_skin}
                               source={require("../../../recourses/sprites/player/Blocks_01_128x128_Alt_02_001.png")}
                            />
                        </View>
                    </TouchableOpacity>
                </View>

                <MenuButton
                    setter={this.props.menuSetter}
                    update={this.props.update}
                    nextView="About" label="About Art"
                />

                <CancelButton
                    setter={this.props.menuSetter}
                    update={this.props.update}
                    nextView="Main" label="↩ Back "
                />

            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        settings: state.settings
    }
};

const mapDispatchToProps = dispatch => {
    const difficultySetter = difficulty => dispatch({
        type: 'DIFFICULTY',
        difficulty: difficulty
    });
    const volumeSetter = volume => dispatch({
        type: 'VOLUME',
        volume: volume
    });
    return {
        difficultySetter: difficultySetter, // <- Not built yet!
        volumeSetter: volumeSetter
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Settings);
