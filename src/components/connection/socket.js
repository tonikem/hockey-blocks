import { NativeModules } from 'react-native';

const socket = NativeModules.socket;

export default socket;
