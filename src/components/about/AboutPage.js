/**
 * All copyrighted material used in the game
 * are listed here. Provides information and
 * links about the creator and license used.
 *
 * The component is used inside `AboutView.js`
 * in a form of a list with navigation buttons.
 * */

import React from "react";
import { View, Text, Image,
        TouchableWithoutFeedback, Linking } from "react-native";

import samples from "./sample_images";
import about from "../../styles/menu/about";
import { CancelButton, ArrowButton } from "../buttons/Buttons.js";

const license_list = (
    require("./made_by_others.json")
).list; // Returns an array of objects.


class AboutPage extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            page_index: 0
        };
    }

// Def function for handling hyperlinks.
    HyperLink = ({ label, link }) => (
        <TouchableWithoutFeedback onPress={ () => {
            Linking.openURL(link)
        }}><View>
            <Text style={about.hyperlink}>
                {label}
            </Text>
        </View>
        </TouchableWithoutFeedback>
    );

    render() {
        const index = this.state.page_index;
        const page = license_list[index];
        const links = page["links"]; // <- Array.
        const CC_icon = links["license-icon"];

        const HyperLink = this.HyperLink;
        const list_length = license_list.length;

        return (
            <View style={about.page_container}>

                <View style={about.flex_row}>
                    <View style={about.flex_col}>

                        <Text style={about.title}>{page.title}</Text>

                        <Text style={about.list_item}>Type: {page["type"]}</Text>
                        <Text style={about.list_item}>Name: {page["name"]}</Text>
                        <Text style={about.list_item}>Author: {page["author"]}</Text>
                        <Text style={about.list_item}>License: {page["license"]}</Text>
                        <Text style={about.list_item}>Changes: {page["changes"]}</Text>

                    </View>

                    <View style={about.flex_col}>

                        <Image source={samples[index]} style={about.icon}/>

                        <HyperLink label="Source" link={links["source"]}/>
                        <HyperLink label="Preview" link={links["preview"]}/>
                        <HyperLink label="Author" link={links["author"]}/>

                    </View>
                </View>

                <TouchableWithoutFeedback
                    onPress={ () => {
                        Linking.openURL(links["license"])
                    }}><Image
                            style={about.license}
                            source={{ uri: CC_icon }}
                        />
                </TouchableWithoutFeedback>

                <View style={about.flex_row}>
                    <ArrowButton
                        label="← prev"
                        func={ () => {
                            this.setState({ page_index:
                                index > 0
                                ? index - 1 : 0
                            })
                        }}/>

                    <Text style={about.index_text}>{index}</Text>

                    <ArrowButton
                        label="next →"
                        func={ () => {
                            this.setState({ page_index:
                                index < list_length -1
                                ? index + 1 : index
                            })
                        }}/>
                </View>

                <CancelButton
                    setter={this.props.menuSetter}
                    update={this.props.update}
                    nextView="Main" label="Back To Menu"
                />

            </View>
        );
    }
}

export default AboutPage;
