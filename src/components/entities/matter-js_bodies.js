import Matter from "matter-js";
import { Dimensions } from "react-native";


export const Matter_root = Matter;

// Overriding html-element reference.
Matter.Common.isElement = () => false;

// Constants.
export const WIDTH = Dimensions.get('window').width;
export const HEIGHT = Dimensions.get('window').height;
export const WALL_SIZE = WIDTH * 0.30513918629550324;
export const MATTER_ENGINE = Matter.Engine;
export const DETECTOR = Matter.Detector;
export const PLAYER_SIZE = WIDTH / 6;
export const VECTOR = Matter.Vector;
export const WORLD = Matter.World;
export const BODY = Matter.Body;


export const puck_body = Matter.Bodies.circle(
    (WIDTH / 2 - WIDTH / 12),  // X
    (HEIGHT / 2 - WIDTH / 12), // Y
    (WIDTH / 12), // Radius
    {
        friction: 0,
        frictionAir: 0,
        frictionStatic: 0,
        restitution: 0.9,
        inertia: 0.01,
        density: 0.1,
        mass: 0.1
    }
);
export const player_body = Matter.Bodies.rectangle(
    (WIDTH / 2 - PLAYER_SIZE / 2), (HEIGHT / 3) * 2,
    PLAYER_SIZE, PLAYER_SIZE,
    {
        friction: 0,
        frictionAir: 0,
        density: 0.1
    }
);
export const opponent_body = Matter.Bodies.rectangle(
    (WIDTH / 2 - PLAYER_SIZE / 2), // opponentX
    (HEIGHT / 3 - PLAYER_SIZE),    // opponentY
    PLAYER_SIZE, PLAYER_SIZE,
    {
        friction: 0,
        frictionAir: 0,
        density: 1
    }
);
export const left_wall_body = Matter.Bodies.rectangle(
    0 - PLAYER_SIZE, // x
    0,               // y
    PLAYER_SIZE,     // width
    HEIGHT * 2,      // height
    {
        isStatic: true,
        density: Infinity,
        mass: Infinity,
        restitution: 0.2
    }
);
export const right_wall_body = Matter.Bodies.rectangle(
    WIDTH,          // x
    0,              // y
    PLAYER_SIZE,    // width
    HEIGHT * 2,     // height
    {
        isStatic: true,
        density: Infinity,
        mass: Infinity,
        restitution: 0.2
    }
);
export const upper_left_wall_body = Matter.Bodies.rectangle(
    PLAYER_SIZE / 3,   // x
    5 - PLAYER_SIZE,   // y
    WALL_SIZE,         // width
    PLAYER_SIZE,       // height
    {
        isStatic: true,
        density: Infinity,
        mass: Infinity,
        restitution: 0.2
    }
);
export const upper_right_wall_body = Matter.Bodies.rectangle(
    WIDTH - WALL_SIZE + PLAYER_SIZE / 3,  // x
    5 - PLAYER_SIZE,      // y
    WALL_SIZE,            // width
    PLAYER_SIZE,          // height
    {
        isStatic: true,
        density: Infinity,
        mass: Infinity,
        restitution: 0.2
    }
);
export const lower_left_wall_body = Matter.Bodies.rectangle(
    PLAYER_SIZE / 3,      // x
    HEIGHT - 5,           // y
    WALL_SIZE,            // width
    PLAYER_SIZE,          // height
    {
        isStatic: true,
        density: Infinity,
        mass: Infinity,
        restitution: 0.2
    }
);
export const lower_right_wall_body = Matter.Bodies.rectangle(
    WIDTH - WALL_SIZE + PLAYER_SIZE / 3,  // x
    HEIGHT - 5,           // y
    WALL_SIZE,            // width
    PLAYER_SIZE,          // height
    {
        isStatic: true,
        density: Infinity,
        mass: Infinity,
        restitution: 0.2
    }
);
