/**
 * `App` is purely used to distinct when
 * the game is on and when it's off. Takes
 * by default to Menu. After the game state
 * is set to `true` in redux store, player
 * will be taken to the chosen game-mode.
 * */

import React from 'react';
import { connect } from "react-redux";

import Menu from './menu/Menu';
import Game from './game/Game';
import socket from "./connection/socket";
import music from "../recourses/audio/music/music";


class App extends React.Component {
    componentDidMount() {
        music.play();
    }

    componentWillUnmount() {
        socket.stop();
    }

    render() {
        if (this.props.game_state.ready) {
            return <Game/>
        } else {
            return <Menu/>
        }
    }
}

/* Redux-store methods */

const mapStateToProps = state => {
    return {
        menu_state: state.menu_state,
        game_state: state.game_state
    }
};

const mapDispatchToProps = dispatch => {
    const startGame = () => dispatch({
        type: 'START_GAME'
    });
    return {
        startGame: startGame
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
