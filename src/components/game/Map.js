/**
 * Returns the current map.
 * */
import React from "react";
import { Image } from "react-native";

import game from "../../styles/game/game";

const maps = [
    require("../../recourses/sprites/maps/ring.png"),
];

const Map = ({ map_number }) => {
    return (
        <Image
            source={maps[map_number]}
            style={game.map}
        />
    )
};

export default Map;
