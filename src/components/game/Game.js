/**
 * The root component for all game-modes.
 * This part needs serious refactoring, but
 * it's parts are very tightly coupled.
 * */

import React from 'react';
import { connect } from "react-redux";
import { View, Text } from "react-native";
import { GameEngine } from "react-native-game-engine";

import socket from "../connection/socket"; // Native module.

import Map from './Map';
import Puck from "../objects/game/Puck";
import Player from "../objects/game/Player";
import Opponent from "../objects/game/Opponent";
import game from "../../styles/game/game";
import basic from "../../styles/basics";
import {
// Root block.
    Matter_root as Matter,

// Matter-js bodies.
    puck_body,
    player_body,
    opponent_body,
    left_wall_body,
    right_wall_body,
    upper_left_wall_body,
    upper_right_wall_body,
    lower_left_wall_body,
    lower_right_wall_body,

// Global constants.
    HEIGHT, WIDTH, PLAYER_SIZE, VECTOR,
    MATTER_ENGINE, WORLD, BODY, DETECTOR
} from '../entities/matter-js_bodies';


// Overriding html-element reference.
Matter.Common.isElement = () => false;

// Turns to `true` when player is touched.
let isMoving = false;

let engine; // <- `Matter-js` engine.

// This determines the `MovePuck` -system.
let isEngineHost;
// If `false`, player will only be receiving coordinates.
// If `true`, the player will be sending coordinates.

// Opponent's coordinates are stored here.
let opponentX = (WIDTH / 2 - PLAYER_SIZE / 2);
let opponentY = (HEIGHT / 3 - PLAYER_SIZE);

// Puck's coordinates received form the server.
let puck_received_X = (WIDTH / 2 - WIDTH / 12);
let puck_received_Y = (HEIGHT / 2 - WIDTH / 12);


class Game extends React.Component {
    constructor() {
        super();
        this.state = {
            map_number: 0,
            isRunning: false
        }
    }

// Handles receiving the coordinates for puck.
    ReceivePuckCoords= (entities) => {
        if (isEngineHost === true
         || isEngineHost === null
         || isEngineHost === undefined
        ) {
            return {...entities};
        }
        let puck = entities["puck"];

        engine.enabled = true;

        BODY.setPosition( puck_body, {
            x: puck._X = puck_received_X,
            y: puck._Y = puck_received_Y
        });

        return {...entities};
    };

// Handles sending the coordinates to other player.
    SendPuckCoords = (entities) => {
        if (isEngineHost === false
         || isEngineHost === null
         || isEngineHost === undefined
        ) {
            return {...entities};
        }

        let puck = entities["puck"];

        // `DETECTOR.collisions` are wrapped in try-catches in case they are undefined.
        try {
            if (DETECTOR.collisions([[player_body, puck_body]], engine)[0].collided) {
                const angle = VECTOR.angle(
                    player_body.position, puck_body.position
                );
                BODY.setVelocity(puck_body,
                    {
                        x: Math.cos(angle) * 3,
                        y: Math.sin(angle) * 3
                    },
                );
            }
        } catch(e) {
            // Continue normally.
        }
        try {
            if (DETECTOR.collisions([[opponent_body, puck_body]], engine)[0].collided) {
                const angle = VECTOR.angle(
                    opponent_body.position, puck_body.position
                );
                BODY.setVelocity(puck_body,
                    {
                        x: Math.cos(angle) * 3,
                        y: Math.sin(angle) * 3
                    },
                );
            }
        } catch(e) {
            // Continue normally.
        }

        puck._X = puck_body.position.x;
        puck._Y = puck_body.position.y;

        return {...entities};
    };

    componentWillMount() {
        socket.setScreenSize(WIDTH, HEIGHT);
        engine = MATTER_ENGINE.create();
        Matter.World.add(WORLD, []);
        this.setState({ isRunning: true });
    }

    componentWillUnmount() {
        clearInterval(this.receiver);
        this.setState({ isRunning: false });

    // Try-catches are for undefined/null values.
        try {socket.stop();} catch(e) {} // <- Close the connection.
        try {engine.stop();} catch(e) {}
        try {MATTER_ENGINE.clear(engine);} catch(e) {} // <- Clearing the running engine.
        try {WORLD.clear(engine.world);} catch(e) {} // <- Clearing all bodies.
        try {socket.reload();} catch(e) {} // <- Reload module.

        isEngineHost = null;

    // Halt player block.
        isMoving = false;

    // Set the puck to it's original coordinates.
        puck_received_X = (WIDTH / 2 - WIDTH / 12);
        puck_received_Y = (HEIGHT / 2 - WIDTH / 12);

    // Also set the opponent's initial coordinates.
        opponentX = (WIDTH / 2 - PLAYER_SIZE / 2);
        opponentY = (HEIGHT / 3 - PLAYER_SIZE);
    }

/* private */ receiver;

    componentDidMount () {
    // Interval for fetching the opponent's
    // coordinates from the native socket-module.
        this.receiver = setInterval(() => {
            try {
                socket.receiveOpponentX(Xcoords => {
                    opponentX = parseInt(Xcoords);
                });
                socket.receiveOpponentY(Ycoords => {
                    opponentY = parseInt(Ycoords);
                });
                socket.receivePuckX(Xcoords => {
                    puck_received_X = parseInt(Xcoords);
                });
                socket.receivePuckY(Ycoords => {
                    puck_received_Y = parseInt(Ycoords);
                });
            }
            catch (e) {
            // When socket object is closed and null.
                this.props.quitGame();
            }
        }, 16);

    // Setting up Matter-js engine and running it.
        engine.world.gravity.y = 0; // <- objects don't fall.
        engine.world.gravity.x = 0;
        WORLD.add(engine.world, [
            puck_body,
            player_body,
            opponent_body,
            left_wall_body,
            right_wall_body,
            upper_left_wall_body,
            upper_right_wall_body,
            lower_left_wall_body,
            lower_right_wall_body,
        ]);
        MATTER_ENGINE.run(engine);

    // Setting the puck body.
        BODY.setPosition( puck_body, {
            x: (WIDTH / 2 - WIDTH / 12),
            y: (HEIGHT / 2 - WIDTH / 12)
        });
    }

// Sets state to true/false for `MovePlayer`.
    move = bool => { isMoving = bool };

// Basic `GameEngine` system for moving player.
    MovePlayer = (entities, {touches}) => {
        let player = entities["player"];
        touches.filter(t => t.type === "move").forEach(t => {
            if (isMoving) {
                // This sets both player's and player_body's coordinates.
                BODY.setPosition(player_body, {
                    x: player._X = player._X + t.delta.pageX,
                    y: player._Y = player._Y + t.delta.pageY
                });
            }
        });

    // Sends player's and puck's coordinates to other client.
        const puck = entities["puck"];
        try {
            socket.send(
                player._X, player._Y, puck._X, puck._Y,
                (error) => {
                    if (error === "err") this.props.quitGame();
                }
            );
        } catch (e) {
        // If Socket is closed.
            this.props.quitGame();
        }
        return {...entities};
    };

// Handles rendering the opponent.
    MoveOpponent = (entities) => {
        let opponent = entities["opponent"];
        BODY.setPosition(opponent_body, {
            x: opponent._X = opponentX,
            y: opponent._Y = opponentY
        });
        return {...entities};
    };

    render() {

        setInterval( ()=> {
            socket.getThreadID( id => {
                if (id !== null && id !== undefined)
                    isEngineHost = id;
            });
        }, 400);

        return (
            <View style={basic.container}>

                <Map map_number={this.state.map_number}/>

                <GameEngine
                    ref={"RNGE"}
                    style={game.area}
                    systems={[
                    // Basic systems for player and opponent.
                    // `MovePlayer` handles collisions with puck.
                        this.MovePlayer,
                        this.MoveOpponent,

                    // Only one of these will be used.
                        this.SendPuckCoords,
                        this.ReceivePuckCoords
                    ]}
                    running={this.state.isRunning}
                    entities={{
                        "player": {
                            _X: (WIDTH / 2 - PLAYER_SIZE / 2),
                            _Y: (HEIGHT / 3) * 2, move: this.move,
                            renderer: <Player/>
                        },
                        "opponent": {
                            _X : opponentX, _Y : opponentY,
                            renderer: <Opponent/>
                        },
                        "puck": {
                            _X : puck_received_X, _Y: puck_received_Y,
                            renderer: <Puck/>
                        },
                }}/>
            </View>
        )
    }
}


/* Redux-store methods */

const mapDispatchToProps = dispatch => {
    const quitGame = () => dispatch({
        type: 'QUIT_GAME'
    });
    return {
        quitGame: quitGame
    }
};

export default connect(
    null, mapDispatchToProps
)(Game);
