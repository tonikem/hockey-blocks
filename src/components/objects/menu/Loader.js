import React from 'react';
import { View, Animated } from 'react-native';

import loader from '../../../styles/menu/loader';


class Loader extends React.Component {
/* private */ animation;

    constructor() {
        super();
        this.state = {
            loadAnim: new Animated.Value(0),
        };
    }

    componentWillMount() {
        const toValue = val => Animated.timing(
            this.state.loadAnim,
            {
                delay: 120,
                toValue: val,
                duration: 1500,
                useNativeDriver: true
            }
        );
        this.animation = Animated.loop(
            Animated.sequence(
                [toValue(1), toValue(0)]
            )
        );
        this.animation.start();
    }

    componentWillUnmount() {
        this.animation.stop();
    }

    render() {
        const rotation = this.state.loadAnim.interpolate({
            inputRange: [0, 1], outputRange: ["-8deg", "8deg"]
        });
        return (
            <View style={loader.container}>
                <Animated.Image
                    source={require("../../../recourses/sprites/player/Blocks_01_128x128_Alt_01_001.png")}
                    style={[ loader.animation, // <- static style.
                    // Stylesheet for Animated rotation:
                        { transform: [{ rotate: rotation }] },
                    ]}/>
            </View>
        )
    }
}

export default Loader;
