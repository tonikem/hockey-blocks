import React from 'react';
import { Image } from 'react-native';

import opponent from '../../../styles/game/opponent';

const source = require( // Change this later.
    "../../../recourses/sprites/player/Blocks_01_128x128_Alt_02_002.png"
);

const Player = ({_X, _Y}) => {
    return (
        <Image
            source={source}
            style={[ opponent.block,
                { left: _X, top: _Y }
            ]}
        />
    );
};

export default Player;
