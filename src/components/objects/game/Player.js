import React from 'react';
import { Image, TouchableWithoutFeedback } from 'react-native';

import player from '../../../styles/game/player';

const source = require(
    "../../../recourses/sprites/player/Blocks_01_128x128_Alt_02_006.png"
);

const Player = ({_X, _Y, move}) => {
    return (
        <TouchableWithoutFeedback
            onPressIn={ () => {
                move(true)
            }}
            onPressOut={ () => {
                move(false)
            }}
            hitSlop={{
                top: 2, left: 2,
                bottom: 2, right: 2
            }}
            pressRetentionOffset={{
                top: 5000, left: 5000,
                bottom: 5000, right: 5000
            }}
        ><Image
            source={source}
            style={[ player.block,
                { top: _Y, left: _X }
            ]}
        />
        </TouchableWithoutFeedback>
    );
};

export default Player;
