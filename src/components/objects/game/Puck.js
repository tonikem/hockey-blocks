import React from 'react';
import { Image } from 'react-native';

import puck from '../../../styles/game/puck';

const source = require(
    "../../../recourses/sprites/disks/disc2.png"
);

const Puck = ({_Y, _X}) => {
    return (
        <Image
            source={source}
            style={[ puck.disk,
                {
                    top: _Y, left: _X
                }
            ]}
        />
    );
};

export default Puck;
