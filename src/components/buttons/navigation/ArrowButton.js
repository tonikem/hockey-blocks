import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import menu from "../../../styles/menu/menu";


const ArrowButton = ({func, label, display}) => (
    <TouchableOpacity onPress={func /*prop function*/}>
        <View elevation={1} style={menu.arrow_button}>
            <Text style={menu.arrow_text}>
                {label}
            </Text>
        </View>
    </TouchableOpacity>
);

export default ArrowButton;
