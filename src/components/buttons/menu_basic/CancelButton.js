import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import menu from "../../../styles/menu/menu";


const CancelButton = ({setter, update, nextView, label}) => (
    <TouchableOpacity onPress={() => { setter(nextView); update();}}>
        <View elevation={1} style={menu.small_button}>
            <Text style={menu.small_text}>
                {label}
            </Text>
        </View>
    </TouchableOpacity>
);

export default CancelButton;
