import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import menu from "../../../styles/menu/menu";


const MenuButton = ({setter, update, nextView, label}) => (
    <TouchableOpacity onPress={ () => {setter(nextView); update();} }>
        <View elevation={1} style={menu.button}>
            <Text style={menu.text}>
                {label}
            </Text>
        </View>
    </TouchableOpacity>
);

export default MenuButton;
