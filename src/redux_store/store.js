import { combineReducers, createStore } from 'redux';

import settingsReducer from './reducers/settingsReducer';
import menuReducer from './reducers/menuReducer';
import gameReducer from './reducers/gameReducer';


// Initializing different parts of the store.

export const initialMenuState = {
    menu: 'Main'
};
export const initialSettingsState = {
    difficulty: 'Normal',
    volume: 0.25
};
export const initialGameState = {
    ready: false // true = game on, false = off.
};


// Combining all reducers into one.

const combinedReducer = combineReducers({
    menu_state: menuReducer,
    settings: settingsReducer,
    game_state: gameReducer
});

const store = createStore(combinedReducer);

export default store;
