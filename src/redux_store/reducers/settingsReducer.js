import { initialSettingsState } from '../store';
import audio from "../../recourses/audio/music/music";


const settingsReducer = (state = initialSettingsState, action) => {
    let newState = {...state};
    switch (action.type) {

        case 'VOLUME':
            newState.volume = action.volume.toFixed(2); // <- String.
            audio.setVolume(action.volume); // Native module ´audio´.
            return newState;

        case 'DIFFICULTY':
            newState.difficulty = action.difficulty;
            return newState;

        default:
            return newState;
    }
};

export default settingsReducer;
