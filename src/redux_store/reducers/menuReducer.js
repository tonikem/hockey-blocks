import { initialMenuState } from '../store';


const menuReducer = (state = initialMenuState, action) => {
    let newState = {...state};
    switch (action.type) {

        case 'MENU_STATE':
            newState.menu = action.menu;
            return newState;

        default:
            return newState;
    }
};

export default menuReducer;
