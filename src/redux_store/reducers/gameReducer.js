import { initialGameState } from '../store';


const gameReducer = (state = initialGameState, action) => {
    let newState = {...state};
    switch (action.type) {

        case 'START_GAME':
            newState.ready = true;
            return newState;

        case 'QUIT_GAME':
            newState.ready = false;
            return newState;

        default:
            return newState;
    }
};

export default gameReducer;
