import { StyleSheet, Dimensions } from "react-native";


const device = Dimensions.get('window');
let menu_font = device.width / 15;

if (menu_font > 36) {
// Sets the maximum font-size.
    menu_font = 36;
}

const start = StyleSheet.create({

    view: {
        width: device.width / 2
    },

    cancel_button: {
        borderWidth: 2,
        borderRadius: 30,
        borderColor: 'white',
        backgroundColor: 'rgba(0, 0, 0, 0.2)',

        shadowColor: 'black',
        shadowOffset: {
            width: 2,
            height: 3
        },
        shadowRadius: menu_font / 2,
        shadowOpacity: 0.9,
    },

    text: {
        padding: 8,
        fontWeight: 'bold',
        fontSize: menu_font * 0.75,
        textAlign: 'center',
        color: 'white',
    },

    loading: {
        paddingLeft: menu_font / 2,
        paddingRight: menu_font / 2,
        fontWeight: 'bold',
        fontSize: menu_font,
        textAlign: 'center',
        color: 'white',
    }
});

export default start;
