import { StyleSheet, Dimensions } from "react-native";


const device = Dimensions.get('window');
let menu_font = device.width / 15;

if (menu_font > 36) {
// Sets the maximum font-size.
    menu_font = 36;
}

const about = StyleSheet.create({
    page_container: {
        flexDirection: 'column'
    },

    flex_row: {
        margin: 6,
        flexDirection: 'row',
        justifyContent: 'center'
    },

    flex_col: {
        //:.
    },

    title: {
        margin: 2,
        fontSize: menu_font * 1.4,
        color: 'white',

        shadowColor: 'black',
        shadowOffset: {
            width: 2,
            height: 3
        },
        shadowRadius: menu_font / 2,
        shadowOpacity: 0.9,
    },

    list_item: {
        fontSize: menu_font / 1.6,
        color: 'white',

        shadowColor: 'black',
        shadowOffset: {
            width: 2,
            height: 3
        },
        shadowRadius: menu_font / 2,
        shadowOpacity: 0.9,
    },

    icon: {
        margin: 12,
        width: menu_font * 1.6,
        height: menu_font * 1.6
    },

    license: {
        margin: 3,
        resizeMode: 'contain',
        height: menu_font
    },

    hyperlink: {
        margin: 8,
        marginLeft: 16,
        fontWeight: 'bold',
        fontSize: menu_font / 1.8,
        textDecorationLine: 'underline',
        color: 'blue',
    },

    index_text: {
        margin: 2,
        paddingLeft: 10,
        paddingRight: 10,
        fontWeight: 'bold',
        fontSize: menu_font,
        textAlign: 'center',
        color: 'white',

        shadowColor: 'black',
        shadowOffset: {
            width: 2,
            height: 3
        },
        shadowRadius: menu_font / 2,
        shadowOpacity: 0.9,
    }
});

export default about;
