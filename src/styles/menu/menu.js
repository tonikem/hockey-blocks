import { StyleSheet, Dimensions } from "react-native";


const device = Dimensions.get('window');
let menu_font = device.width / 15;

if (menu_font > 36) {
// Sets the maximum font-size.
    menu_font = 36;
}

const menu = StyleSheet.create({
// LinearGradient background style.
    mainpage: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },

// Basic view component.
// Used in throughout the menu.
    view: {
        width: device.width / 1.4
    },

    button: {
        margin: 12,
        borderWidth: 3,
        borderRadius: 20,
        borderColor: 'white',
        backgroundColor: 'rgba(0, 0, 0, 0.2)',

        shadowColor: 'black',
        shadowOffset: {
            width: 2,
            height: 3
        },
        shadowRadius: menu_font / 2,
        shadowOpacity: 0.9,
    },

    small_button: {
        margin: 10,
        borderWidth: 3,
        borderRadius: 18,
        borderColor: 'white',
        backgroundColor: 'rgba(0, 0, 0, 0.2)',

        shadowColor: 'black',
        shadowOffset: {
            width: 2,
            height: 3
        },
        shadowRadius: menu_font / 2,
        shadowOpacity: 0.9,
    },

    text: {
        margin: 12,
        paddingLeft: menu_font * 2,
        paddingRight: menu_font * 2,
        fontWeight: 'bold',
        fontSize: menu_font,
        textAlign: 'center',
        color: 'white',

        shadowColor: 'black',
        shadowOffset: {
            width: 2,
            height: 3
        },
        shadowRadius: menu_font / 2,
        shadowOpacity: 0.9,
    },

    small_text: {
        margin: 10,
        paddingLeft: menu_font,
        paddingRight: menu_font,
        fontWeight: 'bold',
        fontSize: menu_font * 0.7,
        textAlign: 'center',
        color: 'white',

        shadowColor: 'black',
        shadowOffset: {
            width: 2,
            height: 3
        },
        shadowRadius: menu_font / 2,
        shadowOpacity: 0.9,
    },

    arrow_button: {
        margin: 4,
        borderWidth: 3,
        borderRadius: 12,
        borderColor: 'white',
        backgroundColor: 'rgba(0, 0, 0, 0.2)',

        shadowColor: 'black',
        shadowOffset: {
            width: 2,
            height: 3
        },
        shadowRadius: menu_font / 2,
        shadowOpacity: 0.9,
    },

    arrow_text: {
        margin: 4,
        paddingLeft: menu_font / 2,
        paddingRight: menu_font / 2,
        fontSize: menu_font * 0.6,
        textAlign: 'center',
        color: 'white',

        shadowColor: 'black',
        shadowOffset: {
            width: 2,
            height: 3
        },
        shadowRadius: menu_font / 2,
        shadowOpacity: 0.9,
    }
});

export default menu;
