import { StyleSheet, Dimensions } from "react-native";


const device = Dimensions.get('window');
let menu_font = device.width / 15;

if (menu_font > 36) {
// Sets the maximum font-size.
    menu_font = 36;
}

const settings = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },

    slider: {
        //:.
    },

    text: {
        fontSize: menu_font,
        textAlign: 'center'
    },

    small_button: {
        margin: 10,
        padding: 6,
        paddingLeft: 28,
        paddingRight: 28,
        borderWidth: 3,
        borderRadius: 18,
        borderColor: 'white',
        backgroundColor: 'rgba(0, 0, 0, 0.2)',

        shadowColor: 'black',
        shadowOffset: {
            width: 2,
            height: 3
        },
        shadowRadius: menu_font / 2,
        shadowOpacity: 0.9
    },

    player_skin: {
        width: menu_font,
        height: menu_font
    }
});

export default settings;
