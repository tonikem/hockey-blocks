import { Dimensions, StyleSheet } from "react-native";


const device = Dimensions.get('window');
let menu_font = device.width / 15;

if (menu_font > 36) {
// Sets the maximum font-size.
    menu_font = 36;
}

const loader = StyleSheet.create({
    container: {
        marginTop: menu_font * 2,
        marginBottom: menu_font * 2,
        padding: 4,
        justifyContent: 'center',
        alignItems: 'center',
    },

    animation: {
        zIndex: 10,
        resizeMode: 'contain',
        width: device.width / 4,
    }
});

export default loader;
