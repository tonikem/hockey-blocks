import { StyleSheet, Dimensions } from "react-native";


const device = Dimensions.get('window');
const opponent_size = device.width / 6;

const opponent = StyleSheet.create({
    block: {
        zIndex: 90,
        position: 'absolute',
        height: opponent_size,
        width: opponent_size
    }
});

export default opponent;
