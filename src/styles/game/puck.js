import { StyleSheet, Dimensions } from "react-native";


const device = Dimensions.get('window');
const puck_size = device.width / 6;

const puck = StyleSheet.create({
    disk: {
        zIndex: 90,
        position: 'absolute',
        height: puck_size,
        width: puck_size
    }
});

export default puck;
