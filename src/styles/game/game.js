import { StyleSheet, Dimensions } from "react-native";


const device = Dimensions.get('window');

const game = StyleSheet.create({
    area: {
        width: device.width,
        height: device.height,
        position: 'absolute'
    },

    map: {
        zIndex: -1,
        resizeMode: 'stretch',
        position: 'absolute',
        width: device.width,
        height: device.height
    }
});

export default game;
