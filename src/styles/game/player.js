import { StyleSheet, Dimensions } from "react-native";


const device = Dimensions.get('window');
const player_size = device.width / 6;

const player = StyleSheet.create({
    block: {
        zIndex: 100,
        position: 'absolute',
        height: player_size,
        width: player_size
    }
});

export default player;
