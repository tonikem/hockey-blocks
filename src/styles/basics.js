import { StyleSheet, Dimensions } from "react-native";


const device = Dimensions.get('window');

const basic = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default basic;
